'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fas';
var iconName = 'id-card-alt';
var width = 576;
var height = 512;
var ligatures = [];
var unicode = 'f47f';
var svgPathData = 'M512 64H384v96H192V64H64C28.7 64 0 92.7 0 128v320c0 35.3 28.7 64 64 64h96v-10.7c0-47.1 38.2-85.3 85.3-85.3h85.3c47.1 0 85.3 38.2 85.3 85.3V512h96c35.3 0 64-28.7 64-64V128c.1-35.3-28.6-64-63.9-64zM288 384c-44.2 0-80-35.8-80-80s35.8-80 80-80 80 35.8 80 80-35.8 80-80 80zm64-352c0-17.7-14.3-32-32-32h-64c-17.7 0-32 14.3-32 32v96h128V32z';

exports.definition = {           
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};
  
exports.faIdCardAlt = exports.definition;           
exports.prefix = prefix;
exports.iconName = iconName; 
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;