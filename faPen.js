'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fas';
var iconName = 'pen';
var width = 512;
var height = 512;
var ligatures = [];
var unicode = 'f304';
var svgPathData = 'M301.2 99.8l111 111c4.7 4.7 4.7 12.3 0 17L140.8 499.2 26.7 511.8C11.4 513.5-1.5 500.6.2 485.3l12.7-114.2L284.2 99.8c4.7-4.7 12.3-4.7 17 0zm196.7-25.6l-60.1-60.1c-18.7-18.7-49.1-18.7-67.9 0l-46.1 46.1c-4.7 4.7-4.7 12.3 0 17l111 111c4.7 4.7 12.3 4.7 17 0l46.1-46.1c18.8-18.8 18.8-49.2 0-67.9z';

exports.definition = {           
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};
  
exports.faPen = exports.definition;           
exports.prefix = prefix;
exports.iconName = iconName; 
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;