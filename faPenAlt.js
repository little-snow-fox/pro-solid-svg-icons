'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fas';
var iconName = 'pen-alt';
var width = 512;
var height = 512;
var ligatures = [];
var unicode = 'f305';
var svgPathData = 'M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zm-85.7 68.7L251 49.7c-9.4-9.4-24.6-9.4-33.9 0L77.8 189c-9.4 9.4-9.4 24.6 0 33.9l11.3 11.3c9.4 9.4 24.6 9.4 33.9 0l111-111 13.4 13.4L12.8 371.2.2 485.3c-1.7 15.3 11.2 28.2 26.5 26.5l114.2-12.7 271.4-271.4c4.6-4.6 4.6-12.2-.1-16.9z';

exports.definition = {           
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};
  
exports.faPenAlt = exports.definition;           
exports.prefix = prefix;
exports.iconName = iconName; 
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;